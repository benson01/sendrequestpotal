﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SendRequestPortal
{
    public class FacilityGuid
    {
        public string facilityGuid { set; get; }
        public string startDate { set; get; }
        public string endDate { set; get; }
    }

    public class Responses
    {
       public  List<Patient> pat = new List<Patient>();

    }
    public class Patient
    {
        public string ArtNumber { set; get; }
        public string FirstName { set; get; }
        public string Surname { set; get; }
        public string sex { set; get; }
        public string DOB { set; get; }
        public string LastClinicalVisit { set; get; }
        public string NextAppointmentDate { set; get; }
        public string mobilephonenumber { set; get; }
        public string StreetName { set; get; }
        public string Response { set; get; }
        public string Action { set; get; }
    }
    public class Client
    {
        public string AppointmnetDate { set; get; }
        public string PatientGuid { set; get; }
        public string Phone { set; get; }
        public string response_value { set; get; }
    }
}
