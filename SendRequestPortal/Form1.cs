﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using System.Windows.Forms;
using Newtonsoft.Json;
using RestSharp;
using Npgsql;

namespace SendRequestPortal
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
           
        }
        public string ConnectionString = "Host=192.168.105.188;Username=asrem;Password=sms;Database=smsnumbers";


        [System.Runtime.InteropServices.DllImport("wininet.dll")]
        public extern static bool InternetGetConnectedState(out int Description, int ReservedValue);
        private void label1_Click(object sender, EventArgs e)
        {

        }
        public void Query_()
        {
            DataTable patientobj = new DataTable();
            patientobj.Columns.Add("FirstName", typeof(string));
            patientobj.Columns.Add("PatientArtNumber", typeof(string));
            patientobj.Columns.Add("NextAppointmentDate", typeof(string));
            patientobj.Columns.Add("mobilephonenumber", typeof(string));
            patientobj.Columns.Add("Response", typeof(string));
            patientobj.Columns.Add("Action", typeof(string));

            NpgsqlConnection conn = new NpgsqlConnection(ConnectionString);
            var sql = @"Select c.patient_guid, d.phone_no , d.appointment_date, r.response_value from public.appointments d
                            left join public.clients c on d.client_id = d.client_id
                            left join public.appointments a on a.client_id = c.client_id
                            left join public.responses r on r.appointment_id = a.id";
            conn.Open();
            if (conn.State == System.Data.ConnectionState.Open)
            {
                using (var cmd = new NpgsqlCommand(sql, conn))
                {
                    int val;
                    NpgsqlDataReader reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        patientobj.Rows.Add(reader[0].ToString(), reader[0].ToString(), reader[0].ToString(), reader[0].ToString());

                    }
                }
            }
            conn.Close();
        }
        private void Form1_Load(object sender, EventArgs e)
        {
           
        }

        public static String facilityGuid()
        {
            var connectionString = @"Server=.\smartcare40;Database=cdc_fdb_db;User Id=sa;Password=m7r@n$4mAz;";
            string FacilityGuild = "";

            string query = "(select FacilityGuid from facility where hmiscode =substring((select value from setting where name = 'hmiscode'),1,8))";
            using (var sqlConnection = new SqlConnection(connectionString))
            {
                sqlConnection.Open();
                using (var sqlCommand = new SqlCommand(query, sqlConnection))
                {
                    SqlDataReader readData = sqlCommand.ExecuteReader();

                    // if data is available
                    if (readData.HasRows)
                    {
                        // call the read
                        while (readData.Read())
                        {

                            FacilityGuild = readData["FacilityGuid"].ToString();



                        }
                    }
                    readData.Close();
                }
            }

            return FacilityGuild;
        }

        public  void Send_request()
        {
            var FacilityGuild = facilityGuid();

            if (dateTimePicker1.Value > dateTimePicker2.Value)
            {

                MessageBox.Show("End date cannot be lesser than Startdate");
            }
            else
            {

                var startDate = this.dateTimePicker1.Value.ToString("yyyy-MM-dd");
                var endDate = this.dateTimePicker2.Value.ToString("yyyy-MM-dd");
                var result = new List<Client>();

                DataTable patientobj = new DataTable();
                patientobj.Columns.Add("ArtNumber", typeof(string));
                patientobj.Columns.Add("Firstname", typeof(string));
                patientobj.Columns.Add("Surname", typeof(string));
                patientobj.Columns.Add("Sex", typeof(string));
                patientobj.Columns.Add("DOB", typeof(string));
                patientobj.Columns.Add("mobilephoneNumber", typeof(string));
                patientobj.Columns.Add("NextPharmacy", typeof(string));
                patientobj.Columns.Add("Last Clinical", typeof(string));
                patientobj.Columns.Add("StreetName", typeof(string));
                patientobj.Columns.Add("response value", typeof(string));
                patientobj.Columns.Add("Action", typeof(string));

                var client = new RestClient("http://css.cidrz.org:85/facility");

                var request = new RestRequest(Method.POST);
                request.AddHeader("accept", "application/json");
                request.AddHeader("content-type", "application/json");
                request.AddHeader("authorization", "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJTbXMgU2VydmljZSBBUEkiLCJpYXQiOm51bGwsImV4cCI6bnVsbCwiYXVkIjoid2luZG93cyBzZXJ2aWNlIiwic3ViIjoiV2luZG93IHNlcnZpY2UiLCJHaXZlbk5hbWUiOiJDaWRyeiBBUEkifQ.Ogvt3o6Kc6dz23j2LouvZGS1soxdPP9FzXp1u3vVxPQ");


                var FacilityGuid = new FacilityGuid()
                {
                    facilityGuid = FacilityGuild,
                    startDate = startDate,
                    endDate = endDate
                };


                var json = JsonConvert.SerializeObject(FacilityGuid);
                request.AddParameter("application/json", json, ParameterType.RequestBody);
                try
                {
                    IRestResponse response = client.Execute(request);
                    var res = response.Content.ToString();
                    if (res != "")
                    {
                        var jsonResult = JsonConvert.DeserializeObject(res).ToString();
                        result = JsonConvert.DeserializeObject<List<Client>>(res);
                    }

                    if (result.Count > 0)
                    {
                        foreach (var item in result)
                        {
                            var patientGuid = item.PatientGuid;
                            Patient pat = retrivefromSmartcare(patientGuid);
                            if (item.response_value.Contains("escheduled"))
                            {
                                patientobj.Rows.Add(pat.ArtNumber,
                                   pat.FirstName,
                                   pat.Surname,
                                   pat.sex,
                                   pat.DOB.Substring(0, 9),
                                   pat.mobilephonenumber,
                                   pat.NextAppointmentDate.Substring(0, 9),
                                   pat.LastClinicalVisit,
                                   pat.StreetName,
                                   item.response_value,
                                   "Call patient to confirm date");
                            }
                            else if (item.response_value.Equals(""))
                            {
                                patientobj.Rows.Add(pat.ArtNumber,
                             pat.FirstName,
                             pat.Surname,
                             pat.sex,
                             pat.DOB.Substring(0, 9),
                             pat.mobilephonenumber,
                             pat.NextAppointmentDate,
                             pat.LastClinicalVisit,
                             pat.StreetName,
                             item.response_value,
                             "Call patient no response");
                            }
                            else
                            {
                                patientobj.Rows.Add(pat.ArtNumber,
                               pat.FirstName,
                               pat.Surname,
                               pat.sex,
                               pat.DOB.Substring(0, 9),
                               pat.mobilephonenumber,
                               pat.NextAppointmentDate,
                               pat.LastClinicalVisit,
                               pat.StreetName,
                               item.response_value,
                               "");
                            }


                        }
                        this.ResponseTable.DataSource = patientobj;

                    }
                    else
                    {
                        MessageBox.Show("No patients on this Day");
                    }


                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }

            }
        }

        public Patient retrivefromSmartcare(string patientGuid)
        {
            Patient obj = new Patient();

            var connectionString = @"Server=.\smartcare40;Database=cdc_fdb_db;User Id=sa;Password=m7r@n$4mAz;";
            var query = @"SELECT	ct.ArtNumber,
		                            ct.FirstName, 
		                            ct.SurName,
		                            ct.sex,
		                            ct.DateOfBirth DOB,
		                            cs.LastClinicalVisit,
		                            ap.NextAppointmentDate [Next Clinical],
		                            coalesce(nullif(a.mobilephonenumber,''), a.phonenumber) mobilephonenumber,
		                            a.StreetName
                            FROM crtRegistrationInteraction ct 
                            JOIN GuidMap g on g.NaturalNumber = CT.patientId_int
                            JOIN Address A ON A.PatientGUID = g.OwningGuid
                            JOIN ClientHivSummary cs on cs.PatientId = ct.patientId_int
                            join
                            (SELECT * FROM
			                            (
				                            SELECT v.PatientId , 
				                            v.NextAppointmentDate , 
				                            ROW_NUMBER()over(partition by patientId order by NextAppointmentDate desc) seq 
				                            FROM crtPharmacyVisit v
				                            WHERE v.ArvDrugDispensed = 1)o
				                            where o.seq =1
				                            )ap on ap.PatientId = ct.patientId_int

                            WHERE g.OwningGuid = '"+ patientGuid + "'";

            using (var sqlConnection = new SqlConnection(connectionString))
            {
                sqlConnection.Open();
                using (var sqlCommand = new SqlCommand(query, sqlConnection))
                {
                    SqlDataReader readData = sqlCommand.ExecuteReader();

                    // if data is available
                    if (readData.HasRows)
                    {
                        // call the read
                        while (readData.Read())
                        {
                            obj.ArtNumber = readData["ArtNumber"].ToString();
                            obj.FirstName = readData["FirstName"].ToString();
                            obj.Surname = readData["SurName"].ToString();
                            obj.sex = readData["sex"].ToString();
                            obj.DOB = readData["DOB"].ToString();
                            obj.LastClinicalVisit = readData["LastClinicalVisit"].ToString();
                            obj.NextAppointmentDate = readData["Next Clinical"].ToString().Substring(0, 10).ToString();
                            obj.mobilephonenumber = readData["mobilephonenumber"].ToString();
                            obj.StreetName = readData["StreetName"].ToString();
                        }
                    }
                    readData.Close();
                }
            }
            return obj;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            int Des;
            bool status = InternetGetConnectedState(out Des, 0);
            if (status)
            {
                Send_request();
            }
            else
            {
                MessageBox.Show("Please connect your computer to the internet.");
            }
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }
    }
}
